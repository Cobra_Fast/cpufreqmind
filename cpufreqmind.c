/*
* Simple program to use on top of the ondemand governor.
* Adjusts the *minimum* frequency according to average usage per core in the defined SLEEP_DURATION.
* The purpose is to provide a little extra performance for recurring burst-like loads which would
* otherwise have to wait for ondemand to scale up the frequency.
*
* Change the constants below and then compile with following command for example:
* gcc -O3 -std=c99 -lcpufreq cpufreqmind.c -o cpufreqmind
*
* 2015 (c) Cobra_Fast <http://ezl.re/>
* CC BY-SA 4.0 <https://creativecommons.org/licenses/by-sa/4.0/>
*/

#define NUM_CORES 8 // Number of processor cores in your system
#define MAX_MHZ 2100 // Maximum minimum frequency you're willing to go up to
#define MIN_MHZ 800 // Minimum minimum frequency
#define UP_THRESH 0.05f // Average load level that needs to be reached to turn up the minimum frequency
#define UP_INCR_MHZ 200 // Step by which the minimum frequency will be raised
#define COUNT_IOWAIT 0 // Whether or not to count IO-wait load into used cpu time
#define SLEEP_DURATION 1 // Length of one interval

#include <stdlib.h>
#include <stdio.h>
#include <assert.h>
#include <unistd.h>
#include <cpufreq.h>

int min(int a, int b)
{
	return (a <= b) ? a : b;
}

int max(int a, int b)
{
	return (a >= b) ? a : b;
}

int main(int argc, char* argv[])
{
	long long unsigned int laststat_used[NUM_CORES];
	long long unsigned int laststat_total[NUM_CORES];
	for (int i = 0; i < NUM_CORES; i++)
	{
		laststat_used[i] = 1;
		laststat_total[i] = 1;
	}
	
	while (1)
	{
		FILE* fstat = fopen("/proc/stat", "r");
		if (fstat == NULL)
		{
			perror("Couldn't open /proc/stat.\n");
			fclose(fstat);
			exit(1);
		}
		
		for (int i = -1; i < NUM_CORES; i++)
		{
			long long unsigned int data[11];
			char buffer[256];
			fgets(buffer, 256, fstat);
			sscanf(
				buffer,
				"cpu%llu %llu %llu %llu %llu %llu %llu %llu %llu %llu %llu",
				&data[0], &data[1], &data[2], &data[3], &data[4], &data[5], &data[6], &data[7], &data[8], &data[9], &data[10]
			);
			if (i == -1)
				continue;
			assert(data[0] == i);
			long long unsigned int cur_total = data[1] + data[2] + data[3] + data[4] + data[5] + data[6] + data[7] + data[8] + data[9] + data[10];
			long long unsigned int cur_used = data[1] + data[2] + data[3] + data[6] + data[7] + data[8] + data[9] + data[10];
			if (COUNT_IOWAIT)
				cur_used += data[5];
			
			if (laststat_total[i] != 1)
			{
				long long unsigned int cur_diff_total = cur_total - laststat_total[i];
				long long unsigned int cur_diff_used = cur_used - laststat_used[i];
				float util = (float)cur_diff_used / (float)cur_diff_total;
				
				struct cpufreq_policy* cpupol;
				cpupol = cpufreq_get_policy(i);
				unsigned long cur_min_freq = cpupol->min / 1000;
				cpufreq_put_policy(cpupol);
				unsigned long new_min_freq = 0;
				
				if (util > UP_THRESH)
				{
					new_min_freq = (cur_min_freq + UP_INCR_MHZ);
					new_min_freq = min(new_min_freq, MAX_MHZ);
				}
				else
				{
					new_min_freq = (cur_min_freq - UP_INCR_MHZ);
					new_min_freq = max(new_min_freq, MIN_MHZ);
				}
				
				if (new_min_freq != cur_min_freq)
				{
					printf("Setting new minimum freq %d for %d (%.0f%% load)\n", new_min_freq, i, util * 100.0f);
					cpufreq_modify_policy_min(i, new_min_freq * 1000);
				}
			}
			
			laststat_total[i] = cur_total;
			laststat_used[i] = cur_used;
		}
		
		fclose(fstat);
		
		sleep(SLEEP_DURATION);
	}
	
	exit(0);
}
